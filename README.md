# README #

This project implement a server using `Node.js` as a backend for a Todo-list app by provideing a certain amount of RestFUL apis for creating, updating and deleting the todo items.

Core modules used are:

* crypto-js for encryption
* express.js for setting up the server
* jsonwebtoken for user authentication
* sequelize, sqlite3 and pg for the persistent layer
* underscore for all the handy functions

## How to test:
This app is also hosted alive at https://boiling-beach-37602.herokuapp.com/.

## Available API:
* POST /users/login
    * login with user details
* Get /todos
    * return the todo items belongs to the current user
* Get /todos/:id
    * return the *particular* todo item belongs to the current user
* Get /todos?completed=true
    * query the completed todo items
* POST /todos
    * add todo item to current user
* POST /users
    * add user to database
* DEL /todos/:id
    * delete a *particular* todo item
* PUT /todos/:id
    * update a *particular* todo item
* DEL /users/login
    * delete a user from database

The available api are also exported using Postman:
[Available API](https://bitbucket.org/cquan/todoapi/downloads/TodoAPI.postman_collection.json)

## How to start
Note: the URL of the app is referred to as `{{apiUrl}}` and the Auth token returned from the server is referred as `{{authToken}}`.
Before testing each API, a user should be created first by the `POST /users` api with the following request details:
```
Headers: {
    Content-Type: application/json
}
Body: {
    "email": "userEmail@live.com",
    "password": "userPassword"
}
```
Then, we can use the registered user detail to login and obtain the auth token:
```
Endpoint: {{apiUrl}}/users/login
Headers: {
    Content-Type: application/json
}
Body: {
    "email": "userEmail@live.com",
    "password": "userPassword"
}
```
Then the server will return a token wth the `Auth` key which should be included for all other user actions.
For example, to add a todo item:
```
Endpoint: {{apiUrl}}/todos
Headers: {
    Content-Type: application/json
    Auth: {{authToken}}
}
Body:
{
"description": "Walk the dog",
"completed": true
}
```

Please refer to the Postman API file for the detailed API usage.
![Screen Shot 2016-08-18 at 7.06.15 PM.png](https://bitbucket.org/repo/9BdqeE/images/1507737446-Screen%20Shot%202016-08-18%20at%207.06.15%20PM.png)